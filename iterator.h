#pragma once

template <typename T, unsigned N>
struct range {
  const T data[N];

  template<typename... Args>
  constexpr range(const Args&... args) : data{args...} {
}
  struct iterator {
    T const* ptr;

    constexpr iterator(const T* ptr) : ptr(ptr) {}
    constexpr iterator(const range<T, N>& arr) : ptr(arr.data) {}
    constexpr iterator(const iterator& it) {
      ptr = it.ptr;
    }

    constexpr void operator++() { ++ptr; }
    constexpr void operator--() { --ptr; }
    constexpr T const& operator* () const { return *ptr; }
    constexpr T const& operator-> () const { return *ptr; }
    constexpr bool operator==(const iterator& rhs) const { return ptr == rhs.ptr; }
    constexpr bool operator!=(const iterator& rhs) const { return !(*this == rhs); }
    constexpr int operator-(const iterator& rhs) const { return ptr - rhs.ptr; }
  };

  constexpr iterator begin() const { return iterator(data); }
  constexpr iterator end()   const { return iterator(data + N); }
};

namespace std {
template<typename forward_it>
constexpr forward_it next(forward_it& it, int n = 1) {
  return forward_it(it.ptr + n);
}

template<typename T>
constexpr typename T::iterator begin(T const& r) {
  return r.begin();
}

template<typename T>
constexpr typename T::iterator end(T const& r) {
  return r.end();
}
}